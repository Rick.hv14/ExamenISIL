﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NUnit.Framework;
using Examen;
using System.IO;

namespace MiLogger.Tests
{
    [TestFixture]
    public class LoggerTests
    {
        private string _mensajeLog = "Examen Final " + DateTime.Now.ToShortDateString()+ " " + DateTime.Now.ToShortTimeString();
        private string ruta = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"], "Log.txt");
        private Logger _logger;
        [SetUp]
        public void InicializarLogger()
        {
            _logger = new Logger();
        }
        [TearDown]
        public void LiberarLogger()
        {
            _logger = null;
        }
        [Test]
        public void Logger_LoggearArchivoTexto_SeLoggeaEnUnTexto()
        {
            ILogger LogTexto = new LogTexto();
            IConsola consolaLog = new LogAlerta();
            _logger.Log(_mensajeLog, LogTexto, consolaLog);
            Assert.IsTrue(File.Exists(ruta));
        }
        [Test]
        [ExpectedException(typeof(SqlException))] // Error de Conexion
        public void Logger_LoggearArchivoTexto_SeLoggeaEnBaseDatosFaltaConexion()
        {
            ILogger LogBaseDatos = new LogBaseDatos();
            IConsola consolaBaseDatos = new LogError();
            _logger.Log(_mensajeLog, LogBaseDatos, consolaBaseDatos);
        }
        [Test]
        public void Logger_LoggearArchivoTexto_SeLoggeaEnConsola()
        {
            ILogger LogBaseDatos = new LogConsola();
            IConsola consolaBaseDatos = new LogInformacion();
            _logger.Log(_mensajeLog, LogBaseDatos, consolaBaseDatos);
        }

    }
}
