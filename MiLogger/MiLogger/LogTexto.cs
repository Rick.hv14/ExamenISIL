﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public class LogTexto : ILogger
    {
        public void Logger(string  MensajeLog,IConsola Nivel)
        {
            string Directorio = System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"];
            string ruta = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"],"Log.txt");
            if (!Directory.Exists(Directorio)) Directory.CreateDirectory(Directorio);
            using (StreamWriter sw = File.AppendText(ruta))
            {
                sw.WriteLine(MensajeLog);
            }
        }
    }
}
