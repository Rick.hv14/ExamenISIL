﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public class LogError : IConsola
    {
        public void EscribirConsola()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
        }
    }
}
