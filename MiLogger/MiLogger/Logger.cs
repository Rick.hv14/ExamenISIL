﻿using System;
using System.Data.SqlClient;
using System.IO;
using MiLogger;

namespace Examen
{
    public class Logger 
    {
        public void Log(string MensajeLog,ILogger Destino,IConsola Nivel)
        {
            Destino.Logger(MensajeLog, Nivel);
        }

    }
}
