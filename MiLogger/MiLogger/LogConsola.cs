﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger 
{
    public class LogConsola : ILogger
    {
        public void Logger(string  MensajeLog,IConsola Nivel)
        {
            Console.WriteLine(DateTime.Now.ToShortDateString() + MensajeLog);
        }
    }
}
